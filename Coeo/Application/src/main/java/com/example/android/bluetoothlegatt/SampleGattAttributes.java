/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.util.Log;

import java.util.HashMap;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class SampleGattAttributes {
    private static HashMap<String, String> attributes = new HashMap();
    public static String DEVICE_CHARACTERISTIC = "a5e9b58b-1291-4d20-b350-6887c2615558";
    public static String DEVICE_SERVICE = "fdcdc743-d8e5-4c1c-8836-fd659e867191";

    static {
        // Sample Services.
        //attributes.put("0000180d-0000-1000-8000-00805f9b34fb", "Device Service");
        attributes.put(DEVICE_SERVICE, "Coeo Device Service");

        // Sample Characteristics.
        attributes.put(DEVICE_CHARACTERISTIC, "Coeo Device Characteristic");
        //attributes.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");
    }

    public static String lookup(String uuid, String defaultName) {
        Log.e("GATTATTRIBUTES", "Looking up UUID - " + uuid);
        Log.e("GATTATTRIBUTES", "Looking up Name - " + defaultName);
        String name = attributes.get(uuid);
        if(name == null || name == "") {
            Log.e("GATTATTRIBUTES", "No Name");
        } else {
            Log.e("GATTATTRIBUTES",name);
        }
        //Log.e("GATTATTRIBUTES",name);
        return name == null ? defaultName : name;
    }
}
